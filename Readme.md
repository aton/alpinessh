Alpine Linux image  with SSH-server
===================================

Version
-------

1.0-beta

Usage
-----

```
docker run -p 2222:22 -v /home/local_user/.ssh/id_rsa.pub:/root/.ssh/authorized_keys:ro -d --name=alpinesshcontainer aleksxp/alpinessh:latest
```

where:

- **-p 2222:22** - exposing port 22
- **-v /home/local_user/.ssh/id_rsa.pub:/root/.ssh/authorized_keys:ro** - adding local key to authorized key of container
- **-d** - run container in background
- **--name=alpinesshcontainer** - name of container (optional)

Change *local_user* to your real local username to use it locally. Or you may create file named **authorized_keys** with desired keys (one key per row) 
and run container like this:

```
docker run -p 2222:22 -v /path/to/authorized_keys:/root/.ssh/authorized_keys:ro -d --name=alpinesshcontainer aleksxp/alpinessh:latest
```

After run you can connect to container:

```
ssh -p 2222 root@localhost
```


Maintainer
----------

Alexander Lebedev <alchimik@gmail.com>

