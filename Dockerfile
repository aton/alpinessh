FROM alpine:latest

MAINTAINER Alexander Lebedev <alchimik@gmail.com>

RUN apk update && apk add bash openssh && rm -rf /var/cache/apk/*

RUN cd /etc/ssh/ && /usr/bin/ssh-keygen -A

COPY sshd_config /etc/ssh/sshd_config

CMD ["/usr/sbin/sshd","-D"]
